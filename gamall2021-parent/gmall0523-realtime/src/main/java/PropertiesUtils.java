/**
 * Author: Clearlove
 * Time: 2021/5/10 21:03
 * Description:
 */
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;
public class PropertiesUtils {
    // 当前class的日志公共对象
    private static final Logger LOGGER = Logger.getLogger(PropertiesUtils.class);

    /**
     * @Description 获取某一个properties文件的properties对象，以方便或得文件里面的值
     *
     * @Author Garrett Wang
     * @param filePath：properties文件所在路径
     * @return Properties对象
     */
    public static Properties getProperties(String filePath) {

        final Properties properties = new Properties();

        try {
            properties.load(PropertiesUtils.class.getClassLoader().getResourceAsStream(filePath));
        } catch (IOException e) {
            LOGGER.error("获取某一个properties文件的properties对象失败", e);
        }

        return properties;

    }

}
