package com.clearlove.gmall.realtime.app

import java.lang
import java.text.SimpleDateFormat
import java.util.Date

import com.alibaba.fastjson.{JSON, JSONObject}
import com.clearlove.gmall.realtime.bean.DauInfo
import com.clearlove.gmall.realtime.utils.{MyESUtil, MyKafkaUtil, MyRedisUtil, OffsetManagerUtil}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.TopicPartition
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.{HasOffsetRanges, OffsetRange}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import redis.clients.jedis.Jedis
import scala.collection.mutable.ListBuffer



/**
 * Author: Clearlove
 * Time: 2021/2/4 19:05
 * Description: 日活用户统计
 */
object DauApp {
  def main(args: Array[String]): Unit = {
    val conf: SparkConf = new SparkConf().setMaster("local[4]").setAppName("DauApp")
    val ssc: StreamingContext = new StreamingContext(conf, Seconds(5))
    var topic: String = "gmall_start_0523"
    var groupId: String = "gmall_dau_0523"

    //通过SparkStreaming程序从kafka中读取数据,没做精准一次性消费之前传数据
    // val kafkaDstrem: InputDStream[ConsumerRecord[String, String]] = MyKafkaUtil.getKafkaStream(topic, ssc, groupId)
    //只拿取启动日志gmall_start_0523的value值格式为json的Dstream
    // val jsonDstream: DStream[String] = kafkaDstrem.map(_.value())
    // jsonDstream.print()


    //精准一次性消费传数据
    //先从Redis中获取kafka分区的偏移量
    val offsetMap: Map[TopicPartition, Long] = OffsetManagerUtil.getOffset(topic, groupId)
    var recordDStream: InputDStream[ConsumerRecord[String, String]] = null
    if (offsetMap != null && offsetMap.size > 0) {
      //如果redis中存在当前消费者组对该主题的偏移量信息，那么就从当前偏移量位置开始消费
      recordDStream = MyKafkaUtil.getKafkaStream(topic, ssc, offsetMap, groupId)
    }
    else {
      //如果redis中不存在当前消费者组信息，则按照配置从最新位置开始消费
      recordDStream = MyKafkaUtil.getKafkaStream(topic, ssc, groupId)
    }


    //获取当前采集周期从kafka中消费的数据的起始位置偏移量和结束位置偏移量
    var offsetRanges: Array[OffsetRange] = Array.empty[OffsetRange]

    val offsetDStream: DStream[ConsumerRecord[String, String]] = recordDStream.transform {
      rdd => {
        //因为recordDStream底层封装的是KafkaRDD，混入了HasOffsetRanges特质
        //这个特质中提供了可以获取偏移量范围的方法

        offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
        rdd
      }
    }


    //对kafkaDstream做一个映射,map是拿到一个json的Dstream,record代表一条数据
    //返回的格式为{"dt":""，"原Dstream":"","hr":"","ts":""}
    val jsonDstrem: DStream[JSONObject] = offsetDStream.map {
      record => {
        //拿到json字符串的值
        val jsonString: String = record.value()
        //用fastjson将json字符串转换为json对象，方便对json操作
        val jsonObject: JSONObject = JSON.parseObject(jsonString)
        //从json对象中获取时间戳,时间戳是long类型的用getlong方法获取
        val ts: lang.Long = jsonObject.getLong("ts")
        //时间工具转换类SimpleDateFormat将时间戳转换为日期和小时  2021-2-5 21
        //format方法获取日期日期用new 一个日期用unit中的Date获取,将ts传进去
        val dateStr: String = new SimpleDateFormat("yyy-MM-dd HH").format(new Date(ts))
        //将时间按空格分词 生成个数组[2020-2-5,21]
        val dateStrArr: Array[String] = dateStr.split(" ")
        val dt: String = dateStrArr(0)
        val hr: String = dateStrArr(1)
        //拿取年月日和小时
        jsonObject.put("dt", dt)
        jsonObject.put("hr", hr)
        //返回结果
        jsonObject
      }
    }



    //调用jsonDstream方法中的print打印结果
    //    jsonDstrem.print(1000)

    //通过Redis对采集数据的启动日志进行去重，利用filter过滤 方案1
    // 弊端：每条数据都要获取一次redis连接，过于频繁[可以以分区去处理]
    //redis 类型用set key:  dau:2021-2-5    value: mid   expire有效日期: 3600*4
    /*  val filteredDstream1: DStream[JSONObject] = jsonDstrem.filter {
        jsonObj => {
          //拿取Dstream中的数据，数据格式为{"dt":""，"原Dstream":"","hr":"","ts":""}
          //dt：为String类型
          val dt: String = jsonObj.getString("dt")
          //mid: mid在common中，common为json所以先获取json在获取String
          val mid: String = jsonObj.getJSONObject("common").getString("mid")
          //拼接key值  key:   dau:2021-2-5
          var dauKey = "dau:" + dt

          //获取jedis客户端
          val jedis: Jedis = MyRedisUtil.getJedisClient

          //判断是否在redis中登录过，可用sadd方法放数据sadd[key,value value]一个K对多个V
          val isFirst: lang.Long = jedis.sadd(dauKey, mid)
          //数据传完后就可以关闭了，拿一次要记得关，他连接有限，会报错
          jedis.close()

          //设置失效时间用expire方法设置会导致错误，若23：00传入数据则要等3600*24失效，每次的采集周期都要执行一次这个方法
          // 所以他会一直重置他的失效时间，它不失效就会保留占用内存
          //jedis.expire(dauKey,3600*24)
          //所以用TTL方法设置,返回负数代表没设置失效，他会判断你是否有了失效时间，有的话就false（然后等它自动失效然后删除）
          if (jedis.ttl(dauKey) < 0) {
            jedis.expire(dauKey, 3600 * 24)
          }
          else {
            false
          }

          if (isFirst == 1L) {
            true
          }
          else {
            false
          }
        }
      }
      */
    //模拟日中时在application.properties中有设备登录最大值，每天最多只有50个用户，count总和为50
    //    filteredDstream1.count().print()


    //通过Redis对采集数据的启动日志进行去重  方案2 以分区为单位对数据进行处理，每一个分区获取一次redis的连接
    //redis 类型用set key:  dau:2021-2-5    value: mid   expire有效日期: 3600*4
    //只有一个值的时候用mapPartitions{}，若有两个参数就不能用。可以考虑mapPartitions()
    val filterDstream2: DStream[JSONObject] = jsonDstrem.mapPartitions {
      jsonObjItr => { //以分区为单位对数据进行处理
        //定义一个集合,用于存放当前分区中第一次登陆的日志
        val filteredList: ListBuffer[JSONObject] = new ListBuffer[JSONObject]()
        //拿了就要想着关闭
        val jedis: Jedis = MyRedisUtil.getJedisClient
        //对分区的数据进行遍历
        for (jsonObj <- jsonObjItr) {
          //获取日期
          val dt: String = jsonObj.getString("dt")
          //获取设备id
          val mid: String = jsonObj.getJSONObject("common").getString("mid")
          //拼接操作redis的key
          var dauKey = "dau:" + dt
          //将值用sadd方式传入redis
          val isFirst: lang.Long = jedis.sadd(dauKey, mid)
          //判断失效时间是否设置，未设置就去设置
          if (jedis.ttl(dauKey) < 0) {
            jedis.expire(dauKey, 3600 * 24)
          }
          if (isFirst == 1L) {
            //若是第一次登录就保存在jsonObj中
            filteredList.append(jsonObj)
          }

        }
        jedis.close()
        //传过来的是一个迭代器要有返回值，返回也应该是可迭代的
        filteredList.toIterator
      }
    }
    //    filterDstream2.count().print()


    //将数据批量保存在ES中
    filterDstream2.foreachRDD { //形容算子，原来的数据是dstream，目的是拿到其中的rdd进行rdd的分区操作
      rdd => { //获取dstream中的rdd
        rdd.foreachPartition { //以分区为单位对rdd中的数据进行处理，方便批量插入
          jsonObjItr => {
            val dauInfoList: List[(String, DauInfo)] = jsonObjItr.map { //对分区内容做一个映射，目的：拿到其中数据的这里是json对象
              jsonObj => { //拿到分区中的每一个rdd中的数据，拿到json对象
                //每次处理的是一个 json 对象 将 json 对象封装为样例类
                val commonJsonObj: JSONObject = jsonObj.getJSONObject("common")
                val dauInfo = DauInfo(
                  commonJsonObj.getString("mid"),
                  commonJsonObj.getString("uid"),
                  commonJsonObj.getString("ar"),
                  commonJsonObj.getString("ch"),
                  commonJsonObj.getString("vc"),
                  //日期和小时在common外面
                  jsonObj.getString("dt"),
                  jsonObj.getString("hr"),
                  "00", //分钟我们前面没有转换，默认 00
                  jsonObj.getLong("ts")
                )
                (dauInfo.mid, dauInfo)
              }
            }.toList //这里将迭代器接口转换为list方便操作
            //将数据批量保存入es
            val dt: String = new SimpleDateFormat("yyyy-MM-dd").format(new Date())
            MyESUtil.bulkInsert(dauInfoList, "gmall0523_dau_info_" + dt)
          }
        }
        OffsetManagerUtil.saveOffset(topic, groupId, offsetRanges)
      }
    }

    ssc.start()
    ssc.awaitTermination()

  }
}
