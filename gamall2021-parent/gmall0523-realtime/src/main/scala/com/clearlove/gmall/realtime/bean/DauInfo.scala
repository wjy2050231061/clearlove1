package com.clearlove.gmall.realtime.bean

/**
 * Author: Clearlove
 * Time: 2021/2/7 9:26
 * Description: 封装日活数据的样例类
 */
case class DauInfo(
                    mid: String, //设备 id
                    uid: String, //用户 id
                    ar: String, //地区
                    ch: String, //渠道
                    vc: String, //版本
                    var dt: String, //日期
                    var hr: String, //小时
                    var mi: String, //分钟
                    ts: Long //时间戳
                  ) {}
