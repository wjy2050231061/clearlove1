package com.clearlove.gmall.realtime.utils

import java.io.InputStreamReader
import java.nio.charset.StandardCharsets
import java.util.Properties

/**
 *
 * 读取properties配置文件的工具类
 */
object MyPropertiesUtil {
  def main(args: Array[String]): Unit = {
    val prop: Properties = MyPropertiesUtil.load("config.properties")
    println(prop.getProperty("kafka.broker.list"))
  }

  //将文件路径封装成一个properties对象，这里不指定具体路径(要打包上传到虚拟机)
  //prop.load方法读取配置文件
  //用thread拿取当前线程的类加载器
  def load(propertiesName: String): Properties = {
    val prop = new Properties();
    prop.load(new InputStreamReader(Thread.currentThread().getContextClassLoader.getResourceAsStream(propertiesName), StandardCharsets.UTF_8))
    prop
  }
}
