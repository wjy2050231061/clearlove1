package com.clearlove.gmall.realtime.utils

import java.sql.{Connection, DriverManager, PreparedStatement, ResultSet, ResultSetMetaData}

import com.alibaba.fastjson.JSONObject

import scala.collection.mutable.ListBuffer

/**
 * Author: Clearlove
 * Time: 2021/5/5 11:34
 * Description: 用于从phoenix中查询数据
 *
 * User_id   if_consumerd
 *   cl           1
 *   ls           1
 *   gs           1
 *
 *   期望结果：
 *   {"user_id":"cl","if_consumerd":"1"}
 *   {"user_id":"cl","if_consumerd":"1"}
 *   {"user_id":"cl","if_consumerd":"1"}
 *
 *
 */
object PhoenixUtil {
  def main(args: Array[String]): Unit = {
    var sql:String = "select * from user_status0523"
    val rs: List[JSONObject] = queryList(sql)
    println(rs)
  }

  def queryList(sql:String): List[JSONObject] = {
    val rsList: ListBuffer[JSONObject] = new ListBuffer[JSONObject]
    //注册驱动
    Class.forName("org.apache.phoenix.jdbc.PhoenixDriver")
    //建立连接
    val conn: Connection = DriverManager.getConnection("jdbc:phoenix:master,work1,work2:2181")
    //创建数据库操作对象
    val ps: PreparedStatement = conn.prepareStatement(sql)
    //执行sql语句
    val rs: ResultSet = ps.executeQuery()
    //处理结果集
    val rsMetaData: ResultSetMetaData = rs.getMetaData
    while (rs.next()){
      val userStatusJsonObj = new JSONObject()
      //{"user_id":"cl","if_consumerd":"1"}
      for(i <- 1 to rsMetaData.getColumnCount){
        userStatusJsonObj.put(rsMetaData.getColumnName(i),rs.getObject(i))
      }
      rsList.append(userStatusJsonObj)
    }
    //释放资源
    rs.close()
    ps.close()
    conn.close()
    rsList.toList
  }
}
