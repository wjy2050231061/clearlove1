package com.clearlove.gmall.realtime.bean

/**
 * Author: Clearlove
 * Time: 2021/5/5 11:27
 * Description: 
 */
case class UserStatus(
                       userId:String, //用户 id
                       ifConsumed:String //是否消费过 0 首单 1 非首单
                     )
