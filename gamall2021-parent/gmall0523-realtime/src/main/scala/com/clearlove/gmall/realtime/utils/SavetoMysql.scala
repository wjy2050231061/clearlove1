package com.clearlove.gmall.realtime.utils

import java.net.InetAddress
import java.sql.DriverManager

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Author: Clearlove
 * Time: 2021/5/9 18:20
 * Description: 
 */
object SavetoMysql {
  def main(args: Array[String]): Unit = {
    //1. 创建sparkConf和SparkContext对象
    val confs = new SparkConf().setAppName("SavetoMysql").setMaster("local[2]")
    val sc = new SparkContext(confs)
    //2. 读取数据分析
//    F:\本学期\假期\sparkstreaming电商日志实时分析
    val resultRDD: RDD[String] = sc.textFile("file:///F:/本学期/假期/sparkstreaming电商日志实时分析/access2.log")
//      .map(line => {
//        val strings = line.split(" ")
//        val url = strings(6)
//        (url, 1)
//      }).reduceByKey(_ + _).sortBy(_._2, false)
    resultRDD
    //.collect().foreach(println)
    print(resultRDD.getClass)

    print(resultRDD)
    // 获取下面这段代码所在被执行的主机
    val hostName = InetAddress.getLocalHost.getHostName
    val address = InetAddress.getLocalHost.getAddress
    val theadId = Thread.currentThread().getId
    val threadName = Thread.currentThread().getName

//    println("==main==" + hostName + ", address: " + address + ", theadId: " + theadId + ", threadName: " + threadName)

    //3. 将分析的结果写入MySQL中

    //4. 关闭资源
    sc.stop()
  }
  def saveToMySQL(resultRDD:RDD[(String,Int)]): Unit ={
    //3.1 将MySQL的驱动依赖添加进来
    resultRDD.foreach(t=>{
      // 获取下面这段代码所在被执行的主机
      val hostName = InetAddress.getLocalHost.getHostName
      val address = InetAddress.getLocalHost.getAddress
      val theadId = Thread.currentThread().getId
      val threadName = Thread.currentThread().getName

//      println("==foreach==" + hostName + ", address: " + address + ", theadId: " + theadId + ", threadName: " + threadName)

      val url = t._1
      val value = t._2
      //3.2 调用MySQL驱动程序api进行编写业务逻辑
      val connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/niit?characterEncoding=UTF-8", "root", "123456")
      // create table log(url varchar(50),total int)
      val preparedStatement = connection.prepareStatement("insert into log values (?,?)")
      preparedStatement.setString(1,url)
      preparedStatement.setInt(2,value)
      // insert into log values("/shop/detail.html?id=402857036a2831e001kshdksdsdk89933",4)
      val result = preparedStatement.executeUpdate()
      // 关闭资源
      if (preparedStatement != null){
        preparedStatement.close()
      }
      if (connection != null){
        connection.close()
      }
    })
  }

}
