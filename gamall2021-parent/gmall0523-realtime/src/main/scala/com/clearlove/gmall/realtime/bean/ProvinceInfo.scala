package com.clearlove.gmall.realtime.bean

/**
 * Author: Clearlove
 * Time: 2021/5/6 20:11
 * Description: 
 */
case class ProvinceInfo(
                         id:String,
                         name:String,
                         area_code:String,
                         iso_code:String
                       )
