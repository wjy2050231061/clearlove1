package com.clearlove.gmall.realtime.utils

/* ES 的连接的代码*/

import java.util

import io.searchbox.client.config.HttpClientConfig
import io.searchbox.client.{JestClient, JestClientFactory}
import io.searchbox.core._
import org.elasticsearch.index.query.{BoolQueryBuilder, MatchQueryBuilder, TermQueryBuilder}
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder
import org.elasticsearch.search.sort.SortOrder

import scala.collection.mutable


object MyESUtil {


  private var jestFactory: JestClientFactory = null;

  def getJestClient: JestClient = {
    if (jestFactory == null) {
      bulid()
    }
    jestFactory.getObject
  }

  def bulid(): Unit = {
    jestFactory = new JestClientFactory
    //这里端口应该是9200  不是5601   5601是kibana操作es的端口
    jestFactory.setHttpClientConfig(new HttpClientConfig.Builder("http://master:9200")
      .multiThreaded(true)
      .maxTotalConnection(20)
      .connTimeout(10000).readTimeout(2000).build())
  }

  //向ES中插入数据 方法一：直接传json
  def putIndex(): Unit = {
    //获取客户端连接
    val jestClient: JestClient = getJestClient
    //定义执行的source
    var source: String =
      """
        |"id":100,
        | "name":"operation meigong river",
        | "doubanScore":8.0,
        | "age":23,
        | "actorList":[
        |  {"id":3,"name":"zhang han yu"}
        |  ]
      """.stripMargin
    val index: Index = new Index.Builder(source)
      .index("movie_index_5")
      .`type`("movie")
      .id("1")
      .build()
    //通过客户端对象操作ES   execute参数为Action类型，Index是Action接口的实现类
    jestClient.execute(index)
    //关闭连接
    jestClient.close()
  }

  //向ES中插入数据 方法二：创建一个样例类对象（将向插入的文档封装为一个样例类对象）
  def putIndex2(): Unit = {
    val jestClient: JestClient = getJestClient

    /*"id":100,
  "name":"operation meigong river",
  "doubanScore":8.0,
  "age":23,
  "actorList":[
  {"id":3,"name":"zhang han yu"}
  ]*/
    val actorList: util.ArrayList[util.Map[String, Any]] = new util.ArrayList[util.Map[String, Any]]()
    val actorMap1: util.HashMap[String, Any] = new util.HashMap[String, Any]()
    actorMap1.put("id", 123456)
    actorMap1.put("name", "Clearlove7")
    actorList.add(actorMap1)

    //封装样例类对象
    val movie: Movie = Movie("123", "clearlove", 9.5f, 23, actorList)
    val index: Index = new Index.Builder(movie)
      .index("movie_index_5")
      .`type`("movie")
      .id("2")
      .build()
    //通过客户端对象操作ES   execute参数为Action类型，Index是Action接口的实现类
    jestClient.execute(index)
    //关闭连接
    jestClient.close()
  }

  //根据文档ID，从ES中查询出一条记录
  def queryIndexById(): Unit = {
    val jestClient: JestClient = getJestClient
    val get: Get = new Get.Builder("movie_index_5", "2").build()
    //    val res:DocumentResult = jestClient.execute(get)
    val res: DocumentResult = jestClient.execute(get)
    println(res.getJsonString)
    jestClient.close()

  }

  //根据指定查询条件，从ES中查询多个文档  方式1
  def queryIndexByCondition1(): Unit = {
    val jestClient: JestClient = getJestClient
    val query: String =
      """
        |{
        |  "query": {
        |    "bool": {
        |       "must": [
        |        {"match": {
        |          "name": "clearlove"
        |        }}
        |      ],
        |      "filter": [
        |        {"term": { "actorList.name.keyword": "Clearlove7"}}
        |      ]
        |    }
        |  },
        |  "from": 0,
        |  "size": 20,
        |  "sort": [
        |    {
        |      "doubanScore": {
        |        "order": "desc"
        |      }
        |    }
        |  ],
        |  "highlight": {
        |    "fields": {
        |      "name": {}
        |    }
        |  }
        |}
      """.stripMargin
    //封装Search对象
    val search: Search = new Search.Builder(query)
      .addIndex("movie_index_5")
      .build()
    val res: SearchResult = jestClient.execute(search)
    //SearchResult#Hit[util.Map[String, Any], Void]查询结果
    val list: util.List[SearchResult#Hit[util.Map[String, Any], Void]] = res.getHits(classOf[util.Map[String, Any]])
    //将java的list转换为json的list,中间经过了scala的转换
    import scala.collection.JavaConverters._
    val resList: mutable.Buffer[util.Map[String, Any]] = list.asScala.map(_.source)
    println(resList.mkString("\n"))
    jestClient.close()
  }

  //根据指定查询条件，从ES中查询多个文档  方式2
  def queryIndexByCondition2(): Unit = {
    val jestClient: JestClient = getJestClient
    //SearchSourceBuilder用于构建查询的json格式字符串
    val searchSourceBuilder: SearchSourceBuilder = new SearchSourceBuilder
    //query下面有个bool，用于构建bool对象拿去到bool下面的must、filter
    val boolQueryBuilder: BoolQueryBuilder = new BoolQueryBuilder()
    boolQueryBuilder.must(new MatchQueryBuilder("name", "clearlove"))
    boolQueryBuilder.filter((new TermQueryBuilder("actorList.name.keyword", "Clearlove7")))
    searchSourceBuilder.query(boolQueryBuilder)
    searchSourceBuilder.from(0)
    searchSourceBuilder.size(10)
    searchSourceBuilder.sort("doubanScore", SortOrder.ASC)
    searchSourceBuilder.highlighter(new HighlightBuilder().field("name"))
    val query: String = searchSourceBuilder.toString
    //println(query)


    //封装Search对象
    val search: Search = new Search.Builder(query).addIndex("movie_index_5").build()
    val res: SearchResult = jestClient.execute(search)
    //SearchResult#Hit[util.Map[String, Any], Void]查询结果
    val resList: util.List[SearchResult#Hit[util.Map[String, Any], Void]] = res.getHits(classOf[util.Map[String, Any]])
    //调用java集合转Scala集合的一个包，因为Scala集合可操作的功能要多一点
    import scala.collection.JavaConverters._
    val list: List[util.Map[String, Any]] = resList.asScala.map(_.source).toList
    println(list.mkString("\n"))
    jestClient.close()


  }


  /**
   * 向ES中批量插入数据
   *
   * @param dauInfoList
   * @param IndexName
   */
  def bulkInsert(dauInfoList: List[(String, Any)], IndexName: String): Unit = {
    //判断dauInfoList是否为空，有时会出现没数据的情况（没接收到数据的时候）
    if (dauInfoList != null && dauInfoList.size != 0) {
      //获取es客户端
      val jestClient: JestClient = getJestClient
      //构建批次操作
      val bulkBuild: Bulk.Builder = new Bulk.Builder()
      //对批次操作的数据进行遍历
      for ((id, dauInfo) <- dauInfoList) {
        val index: Index = new Index.Builder(dauInfo)
          .index(IndexName)
          .id(id)
          .`type`("_doc")
          .build()
        //将每条数据添加到批量操作中
        bulkBuild.addAction(index)
      }
      //Bulk是Action的实现类，主要实现批量操作
      val bulk: Bulk = bulkBuild.build()
      //执行批量操作获取数据
      val bulkResult: BulkResult = jestClient.execute(bulk)
      //通过执行结果，获取批量插入的数据
      val items: util.List[BulkResult#BulkResultItem] = bulkResult.getItems
      println("向ES中插入" + items.size() + "条数据")
      jestClient.close()
    }


  }

  def main(args: Array[String]): Unit = {
    queryIndexByCondition2()
  }


  case class Movie(id: String, name: String, doubanScore: Float, age: Int, actorList: util.List[util.Map[String, Any]]) {}

}
