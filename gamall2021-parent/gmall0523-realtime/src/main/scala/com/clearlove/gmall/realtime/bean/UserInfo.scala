package com.clearlove.gmall.realtime.bean

/**
 * Author: Clearlove
 * Time: 2021/5/6 20:11
 * Description: 
 */
case class UserInfo(
                     id:String,
                     user_level:String,
                     birthday:String,
                     gender:String,
                     var age_group:String, //年龄段
                     var gender_name:String //性别
                   )
