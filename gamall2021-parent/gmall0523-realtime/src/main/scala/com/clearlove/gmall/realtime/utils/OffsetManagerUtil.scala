package com.clearlove.gmall.realtime.utils

import java.util

import org.apache.kafka.common.TopicPartition
import org.apache.spark.streaming.kafka010.OffsetRange
import redis.clients.jedis.Jedis

/**
 * Author: Clearlove
 * Time: 2021/2/7 21:47
 * Description: 维护偏移量的工具类
 */
object OffsetManagerUtil {
  //从redis中获取偏移量
  //type：hash  key：offset：topic：groupId   fidld：partition   value：偏移量
  def getOffset(topic: String, groupId: String): Map[TopicPartition, Long] = {
    //获取jedis客户端
    val jedis: Jedis = MyRedisUtil.getJedisClient
    //拼接操作redis的key    得到offset:topic:groupId
    val offsetKey = "offset" + topic + ":" + groupId
    //获取当前消费者组消费的主题对应的分区以及偏移量
    val offsetMap: util.Map[String, String] = jedis.hgetAll(offsetKey)
    //关闭jedis客户端
    jedis.close()

    //需要返回的是一个Map[TopicPartition,Long]，所以要对util.Map[String, String]进行操作
    //但是这里是java的map，转成scala的map好操作些
    import scala.collection.JavaConverters._
    val oMap: Map[TopicPartition, Long] = offsetMap.asScala.map {
      case (partition, offset) => {
        println("读取分区偏移量：" + partition + ":" + offset)
        //Map[TopicPartition,Long]
        (new TopicPartition(topic, partition.toInt), offset.toLong)
      }
    }.toMap

    oMap //toMap返回一个不可变的Map[TopicPartition,Long]
  }

  //将偏移量信息保存到Redis中
  def saveOffset(topic: String, groupId: String, offsetRanges: Array[OffsetRange]): Unit = {
    //拼接redis中操作偏移量的key
    var offsetKey = "offset:" + topic + ":" + groupId
    //定义java的map集合，用于存放每个分区对应的偏移量
    val offsetMap: util.HashMap[String, String] = new util.HashMap[String, String]()
    for (offsetRange <- offsetRanges) {
      val partitionId: Int = offsetRange.partition
      val fromOffset: Long = offsetRange.fromOffset
      val untilOffset: Long = offsetRange.untilOffset
      offsetMap.put(partitionId.toString, untilOffset.toString)
      println("保存分区" + partitionId + ":" + fromOffset + "----->" + untilOffset)
    }
    val jedis: Jedis = MyRedisUtil.getJedisClient
    //将分区对应的偏移量,和分区保存在redis
    jedis.hmset(offsetKey, offsetMap)
    jedis.close()
  }

}
