package com.clearlove.gmall.publisher.service;

import java.util.Map;

/**
 * Author: Clearlove
 * Time: 2021/2/27 9:39
 * Description: 操作ES接口
 */
public interface ESService {
    //查询某天的日活数
    public Long getDauTotal(String date);

    Map<String,Long> getDauHour(String date);
}
