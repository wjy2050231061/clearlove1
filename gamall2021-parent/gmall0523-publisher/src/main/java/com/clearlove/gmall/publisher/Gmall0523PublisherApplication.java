package com.clearlove.gmall.publisher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Gmall0523PublisherApplication {

    public static void main(String[] args) {
        SpringApplication.run(Gmall0523PublisherApplication.class, args);
    }

}
