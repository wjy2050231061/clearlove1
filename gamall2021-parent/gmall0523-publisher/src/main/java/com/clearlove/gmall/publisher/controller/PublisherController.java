package com.clearlove.gmall.publisher.controller;

import com.clearlove.gmall.publisher.service.ESService;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Author: Clearlove
 * Time: 2021/2/27 9:38
 * Description: 发布数据接口
 */
//浪费时间2hours
//由于没加这个注解访问网页出现This application has no configured error view, so you are seeing this as a fallback.
//@RestController = @Controller + @ResponseBody 方法返回Object，底层会转换为json格式字符串进行相应处理
@RestController
public class PublisherController {
    //这里的ESService不是操作接口而是自带的
    @Autowired
    ESService esService;
    /*
    访问路径
    http://publisher:8070/realtime-total?date=2019-02-01

    响应数据
    [{"id":"dau","name":"新增日活","value":1200},
    {"id":"new_mid","name":"新增设备","value":233}]
    */
    @GetMapping("realtime-total")
    public Object realtimeTotal(@RequestParam("date") String dt){
        List<Map<String, Object>> rsList = new ArrayList<>();
        Map<String, Object> dauMap = new HashMap<>();
        dauMap.put("id","dau");
        dauMap.put("name","新增日活");
        Long dauTotal = esService.getDauTotal(dt);
        if(dauTotal == null){
            dauMap.put("value",0L);
        }else {
            dauMap.put("value",dauTotal);
        }
        rsList.add(dauMap);

        Map<String, Object> midMap = new HashMap<>();
        midMap.put("id","new_mid");
        midMap.put("name","新增设备");
        midMap.put("value",666);
        rsList.add(midMap);

        return rsList;
    }

    /*
    访问路径
    http://publisher:8070/realtime-hour?id=dau&date=2019-02-01

    分时统计
    {"yesterday":{"11":383,"12":123,"17":88,"19":200 },
    "today":{"12":38,"13":1233,"17":123,"19":688 }}
    */
    @RequestMapping("/realtime-hour")
    public Object realtimeHour(@RequestParam("id") String id, @RequestParam("date") String dt) {
        Map<String,Map<String,Long>> rsMap = new HashMap<>();
        //获取今天的日活信息
        Map<String,Long> tdMap = esService.getDauHour(dt);
        rsMap.put("today",tdMap);

        //获取昨天的日活信息
        //根据当前日期获取昨天日期字符串     今天日期-1
        String yd = getYd(dt);
        Map<String,Long> ydMap = esService.getDauHour(yd);
        rsMap.put("yesterday",ydMap);

        return rsMap;
    }
    private String getYd(String td){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String yd = null;
        try {
            Date tdDate = dateFormat.parse(td);
            Date ydDate = DateUtils.addDays(tdDate, -1);
            yd = dateFormat.format(ydDate);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("日期格式转变失败");
        }
        return yd; }

}
