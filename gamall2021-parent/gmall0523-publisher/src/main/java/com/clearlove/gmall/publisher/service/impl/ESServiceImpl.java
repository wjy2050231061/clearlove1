package com.clearlove.gmall.publisher.service.impl;

import com.clearlove.gmall.publisher.service.ESService;
import io.searchbox.client.JestClient;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.Validate;
import io.searchbox.core.search.aggregation.TermsAggregation;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.support.ValueType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Au   thor: Clearlove
 * Time: 2021/2/27 9:39
 * Description:
 */

//@Controller //（注解）将对象的创建交给Spring容器.方法返回String，默认会当做跳转页面处理

//@Service将当前对象的创建交给Spring容器进行管理
@Service
public class ESServiceImpl implements ESService {
    @Autowired
    JestClient jestClient;
    @Override
    public Long getDauTotal(String date) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(new MatchAllQueryBuilder());
        String query = searchSourceBuilder.toString();
        String indexName="gmall0523_dau_info_"+date+"-query";
        Search search = new
                Search.Builder(query).addIndex(indexName).addType("_doc").build();
        Long total=0L;
        try {
            SearchResult searchResult = jestClient.execute(search);
            if(searchResult.getTotal()!=null){
                total = searchResult.getTotal();
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("查询 ES 异常");
        }
        return total;
    }

    @Override
    public Map<String,Long> getDauHour(String date){
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //方法二：查询terms   AggregationBuilders.terms("groupBy_hr").field("hr").size(24);
        TermsAggregationBuilder termsAggregationBuilder = new TermsAggregationBuilder("groupBy_hr", ValueType.LONG).field("hr").size(24);
        searchSourceBuilder.aggregation(termsAggregationBuilder);
        String query = searchSourceBuilder.toString();
        String indexName="gmall0523_dau_info_"+date+"-query";
        Search search = new Search.Builder(query)
                .addIndex(indexName)
                .addType("_doc")
                .build();
        try {
            SearchResult result = jestClient.execute(search);
            HashMap<String, Long> aggMap = new HashMap<>();
            TermsAggregation termsAgg = result.getAggregations().getTermsAggregation("groupBy_hr");
            if(termsAgg!=null){
                List<TermsAggregation.Entry> buckets = termsAgg.getBuckets();
                for(TermsAggregation.Entry bucket:buckets){
                    aggMap.put(bucket.getKey(),bucket.getCount());
                }
            }
            return aggMap;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("查询异常");
        }

    }
}
