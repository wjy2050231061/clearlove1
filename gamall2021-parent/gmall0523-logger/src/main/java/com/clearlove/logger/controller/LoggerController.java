package com.clearlove.logger.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.util.JSONPObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//@Controller //（注解）将对象的创建交给Spring容器.方法返回String，默认会当做跳转页面处理
//@RestController = @Controller + @ResponseBody 方法返回Object，底层会转换为json格式字符串进行相应处理
//@Slf4j返回构造的日志程序的类别。默认情况下，它将使用注释所在的类型
@RestController
@Slf4j
public class LoggerController {
    //利用Spring提供的对kafka的支持进行连接
    @Autowired   //将KafkaTemplate注入到RestController中(将属性注入到类中)
    KafkaTemplate kafkaTemplate;


    //mock.url=http://localhost:8080/applog
    //提供一个方法，处理模拟器生成的数据
    //RequestMapping 把applog请求，交给方法进行处理
    //RequestBody表示从请求体中获取数据
    @RequestMapping("/applog")
    public String applog(@RequestBody String mockLog){
        System.out.println(mockLog);
        //Log将随机日志保存在磁盘（落盘）
        log.info(mockLog);
        //根据日志类型，发送到kafka不同的主题中去
        //先将接收到的字符串转换成json对象
        JSONObject jsonObject = JSON.parseObject(mockLog);
        JSONObject startJson = jsonObject.getJSONObject("start");
        if(startJson != null) {
            //启动日志
            kafkaTemplate.send("gmall_start_0523", mockLog);
        }else {
            //事件日志
            kafkaTemplate.send("gmall_event_0523",mockLog);
        }
        return "success";
    }
}
