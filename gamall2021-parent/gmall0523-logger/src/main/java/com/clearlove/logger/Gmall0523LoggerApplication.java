package com.clearlove.logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


//程序执行的入口（启动类）
@SpringBootApplication
public class Gmall0523LoggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Gmall0523LoggerApplication.class, args);
    }

}
